import 'package:flutter/material.dart';

class CounterFunctionsScreen extends StatefulWidget {
  const CounterFunctionsScreen({super.key});

  @override
  State<CounterFunctionsScreen> createState() => _CounterFunctionsScreenState();
}

class _CounterFunctionsScreenState extends State<CounterFunctionsScreen> {
  int clickCounter = 0;
  String valueClicks(int clickCounter) =>
      clickCounter != 1 ? "Clicks" : "Click";

  void _counterZero() {
    setState(() {
      clickCounter = 0;
    });
  }

  void _decrement() => setState(() {
        if (!clickCounter.isNegative) {
          clickCounter--;
        } else {
          clickCounter = 0;
        }
      });

  void _increment() {
    setState(() {
      if (!clickCounter.isNegative) {
        if (clickCounter == 10) {
          clickCounter = 0;
        } else {
          clickCounter++;
        }
      } else {
        clickCounter = 0;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).colorScheme.inversePrimary,
          title: const Text("Counter Function Screen"),
          actions: [
            IconButton(
              icon: const Icon(Icons.refresh_rounded),
              onPressed: () {
                setState(() {
                  clickCounter = 0;
                });
              },
            )
          ],
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FilledButton(onPressed: () {}, child: const Text("click me")),
              const SizedBox(
                height: 10,
              ),
              Text('$clickCounter',
                  style: const TextStyle(
                      fontSize: 100, fontWeight: FontWeight.w100)),
              Text(valueClicks(clickCounter),
                  style: const TextStyle(
                      fontSize: 50, fontWeight: FontWeight.w100))
            ],
          ),
        ),
        floatingActionButton: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            CustomFloatingButton(
              icon: Icons.refresh,
              onPressedEvent: () {
                _counterZero();
              },
            ),
            const SizedBox(
              height: 15,
            ),
            CustomFloatingButton(
              icon: Icons.plus_one,
              onPressedEvent: () {
                _increment();
              },
            ),
            const SizedBox(
              height: 15,
            ),
            CustomFloatingButton(
              icon: Icons.exposure_minus_1_outlined,
              onPressedEvent: () {
                _decrement();
              },
            ),
          ],
        ));
  }
}

class CustomFloatingButton extends StatelessWidget {
  final IconData icon;
  final VoidCallback? onPressedEvent;

  const CustomFloatingButton(
      {super.key, required this.icon, this.onPressedEvent});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
        shape: const StadiumBorder(),
        onPressed: onPressedEvent,
        child: Icon(icon));
  }
}
