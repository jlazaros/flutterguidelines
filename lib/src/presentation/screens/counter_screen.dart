import 'package:flutter/material.dart';

class CounterScreen extends StatefulWidget {
  const CounterScreen({super.key});

  @override
  State<CounterScreen> createState() => _CounterScreenState();
}

class _CounterScreenState extends State<CounterScreen> {
  int clickCounter = 0;
  String valueClicks(int clickCounter) =>
      clickCounter != 1 ? "Clicks" : "Click";

  void _increment() {
    setState(() {
      if (!clickCounter.isNegative) {
        if (clickCounter == 10) {
          clickCounter = 0;
        } else {
          clickCounter++;
        }
      } else {
        clickCounter = 0;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Counter Fun Screen"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('$clickCounter',
                style: const TextStyle(
                    fontSize: 100, fontWeight: FontWeight.w100)),
            Text(valueClicks(clickCounter),
                style:
                    const TextStyle(fontSize: 50, fontWeight: FontWeight.w100))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: _increment, child: const Icon(Icons.plus_one)),
    );
  }
}
